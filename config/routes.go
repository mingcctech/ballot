package config

import (
	"ballot/controllers"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
)

func InitRouter(e *echo.Echo) {
	//e.GET("/", Home)
	e.Use(middleware.CORS())
	e.GET("/ballots", controllers.GetBallots)
	e.POST("/ballot", controllers.PostBallot)
	e.Static("/", "public")
}
