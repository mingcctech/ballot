var apiAccessPoint = "http://localhost:1323"

$( document ).ready(function() {
    var rouletter = $('div.roulette');
	var option = {
		speed : 500,
		duration : 1,
		stopImageNumber : 35,
		startCallback : function() {
			$("audio#machine-rolled").trigger("load");
			$("audio#machine-rolled").trigger("play");
		},
		slowDownCallback : function() {
			$("audio#machine-rolling").trigger("load");
			$("audio#machine-rolling").trigger("play");
		},
		stopCallback : function($stopElm) {
			flag = true;
			$("audio#machine-rolled").trigger("load");
			$("audio#machine-rolled").trigger("play");
		}
	}
	rouletter.roulette(option);

    var availableTags = [
		"Jerry", "Terrence", "Sun", "Alpha", "Adam", "Sylvia", "Ray", "Erica", "Andre", "Kyle", "Alan", "Ed", "Sampson"
    ];

$( function() {
    $( "#username" ).autocomplete({
      source: availableTags
    });
  } );

$(".ballot").click(function() {
	username = $("#username").val()

	var letterPattern = /^[a-zA-Z]+$/;

	if (username.match(letterPattern) && availableTags.map(el => el.toLowerCase()).includes(username.toLowerCase())) {
		var settings = {
		  "async": true,
		  "crossDomain": true,
		  "url": `${apiAccessPoint}/ballot`,
		  "method": "POST",
		  "headers": {
			"Content-Type": "application/json",
			"Cache-Control": "no-cache",
		  },
		  "processData": false,
		  "data": JSON.stringify({
				username: username
		  })
		}

		$.ajax(settings).done(function (response) {
			if (response.status.code != "200") {
				return;
			}

			option.stopImageNumber = response.data.Number + 1;
			rouletter.roulette('option', option);
			rouletter.roulette('start');
		});
	}
})
})
