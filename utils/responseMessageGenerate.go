package utils

import (
	"time"
)

type Response struct {
	Data   interface{} `json:"data"`
	Status struct {
		Code     string    `json:"code"`
		Message  string    `json:"message"`
		Datetime time.Time `json:"datetime"`
	} `json:"status"`
}

func GenerateResponseStruct(data interface{}, code string, message string) *Response {
	response := new(Response)
	response.Data = data
	response.Status.Code = code
	response.Status.Message = message
	response.Status.Datetime = time.Now()

	return response
}
