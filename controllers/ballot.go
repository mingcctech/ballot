package controllers

import (
	"ballot/models"
	"ballot/utils"
	_ "fmt"
	"github.com/labstack/echo"
	"net/http"
	"strings"
	"time"
)

func GetBallots(c echo.Context) error {
	ballots, err := models.FindAllBallot()

	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, utils.GenerateResponseStruct(ballots, "200", "OK"))
}

func PostBallot(c echo.Context) error {
	user := new(models.User)

	err := c.Bind(user)

	if err != nil {
		return err
	}

	err = c.Validate(user)

	if err != nil {
		return err
	}

	user.Username = strings.ToLower(user.Username)

	_, err = models.FindOneUserByUsername(user.Username)

	if err != nil {
		return err
	}

	exists, err := models.FindOneBallotByUsername(user.Username)

	if exists.Username == user.Username {
		return c.JSON(http.StatusOK, utils.GenerateResponseStruct(exists, "200", "OK"))
		//return c.JSON(http.StatusOK, utils.GenerateResponseStruct("", "500", "test"))
	}

	ballots, err := models.FindAllBallotByUsername("")

	if err != nil {
		return err
	}

	ballot := ballots[utils.RandomInt(len(ballots))]
	ballot.Username = user.Username
	ballot.UpdatedAt = time.Now()

	err = models.UpdateBallot(ballot)

	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, utils.GenerateResponseStruct(ballot, "200", "OK"))
	//return c.JSON(http.StatusOK, ballot)
}
