package main

import (
	"ballot/models"
	"gopkg.in/mgo.v2/bson"
	"time"
)

func main() {
	//users := []string{"ming", "alison", "jerry", "terrence", "sun", "alpha", "adam", "sylvia", "ray", "erica", "andre", "kyle", "alan", "ed", "sampson"}
	users := []string{"jerry", "terrence", "sun", "alpha", "adam", "sylvia", "ray", "erica", "andre", "kyle", "alan", "ed", "sampson"}
	result := []bool{true, false, false, false, false, false, true, true, true, false, false, true, false}

	for _, element := range users {
		models.InsertUser(models.User{Id: bson.NewObjectId(), Username: element})
	}

	for index, element := range result {
		models.InsertBallot(models.Ballot{Id: bson.NewObjectId(), Number: index, Username: "", Result: element, CreatedAt: time.Now(), UpdatedAt: time.Now()})
	}
}
