package models

import (
	mgo "gopkg.in/mgo.v2"
)

const (
	hostname = "172.77.0.33"
	database = "admin"
	username = "root"
	password = "Hello,world!"
)

var dbSession *mgo.Session

func init() {
	dialInfo := &mgo.DialInfo{
		Addrs:    []string{hostname},
		Source:   database,
		Username: username,
		Password: password,
	}

	session, err := mgo.DialWithInfo(dialInfo)

	if err != nil {
		// Error Handler
	}

	dbSession = session
}

func connect(db, collection string) (*mgo.Session, *mgo.Collection) {
	mSession := dbSession.Copy()
	mCollection := mSession.DB(db).C(collection)
	return mSession, mCollection
}

func FindOne(db, collection string, query, selector, result interface{}) error {
	mSession, mCollection := connect(db, collection)
	defer mSession.Close()
	return mCollection.Find(query).Select(selector).One(result)
}

func FindAll(db, collection string, query, selector, result interface{}) error {
	mSession, mCollection := connect(db, collection)
	defer mSession.Close()
	return mCollection.Find(query).Select(selector).All(result)
}

func Update(db, collection string, query, update interface{}) error {
	mSession, mCollection := connect(db, collection)
	defer mSession.Close()
	return mCollection.Update(query, update)
}

func Insert(db, collection string, documents ...interface{}) error {
	mSession, mCollection := connect(db, collection)
	defer mSession.Close()
	return mCollection.Insert(documents...)
}
