package models

import (
	"gopkg.in/mgo.v2/bson"
	"time"
)

type Ballot struct {
	Id        bson.ObjectId `bson:"_id,omitempty" json:"-"`
	Number    int           `json: "number"`
	Username  string        `json: "username"`
	Result    bool          `json: "result"`
	CreatedAt time.Time     `json: "created_at"`
	UpdatedAt time.Time     `json: "updated_at"`
}

const (
	ballotDatabase   = "Ballot"
	ballotCollection = "ballot"
)

func UpdateBallot(ballot Ballot) error {
	return Update(ballotDatabase, ballotCollection, bson.M{"_id": ballot.Id}, ballot)
}

func InsertBallot(ballot Ballot) error {
	return Insert(ballotDatabase, ballotCollection, ballot)
}

func FindAllBallot() ([]Ballot, error) {
	var results []Ballot
	err := FindAll(ballotDatabase, ballotCollection, bson.M{}, nil, &results)
	return results, err
}

func FindAllBallotByUsername(username string) ([]Ballot, error) {
	var results []Ballot
	err := FindAll(ballotDatabase, ballotCollection, bson.M{"username": username}, nil, &results)
	return results, err
}

func FindOneBallotByUsername(username string) (Ballot, error) {
	var result Ballot
	// TODO: refactoring FindAll().limit(1)
	err := FindOne(ballotDatabase, ballotCollection, bson.M{"username": username}, nil, &result)
	return result, err
}
