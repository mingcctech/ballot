package models

import (
	"gopkg.in/mgo.v2/bson"
)

const (
	userDatabase   = "Ballot"
	userCollection = "user"
)

type User struct {
	Id       bson.ObjectId `bson:"_id,omitempty" json:"-"`
	Username string        `bson:"username" json:"username" validate:"required,alpha,gt=0"`
}

func InsertUser(user User) error {
	return Insert(userDatabase, userCollection, user)
}

func FindOneUserByUsername(username string) (User, error) {
	var result User
	err := FindOne(userDatabase, userCollection, bson.M{"username": username}, nil, &result)
	return result, err
}
